#ifndef GAUDIALG_TUPLES_H
#define GAUDIALG_TUPLES_H 1
// ============================================================================
// GaudiAlg
// ============================================================================
#include "GaudiAlg/TupleObj.h"
// ============================================================================

/** @namespace Tuples Tuples.h GaudiAlg/Tuples.h
 *  helper namespace to collect useful definitions, types,
 *  constants and functions, related to manipulations
 *  with N-Tuples and event tag collections
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date   2004-01-23
 */
namespace Tuples
{
  class Tuple;
  class TupleObj;
} // end of namespace Tuples

#endif // GAUDIALG_TUPLES_H
